package com.tyurin.generator;

import com.tyurin.generator.annotation.Column;
import com.tyurin.generator.annotation.Entity;
import com.tyurin.generator.annotation.Id;
import com.tyurin.generator.annotation.Table;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;

//"SELECT name, price, date FROM product;"
public class DefaultQueryGenerator implements QueryGenerator {
    private final List<String> columnsName = new ArrayList<>();
    private final String SELECT = "SELECT ";
    private final String FROM = " FROM ";
    private final String WHERE = " WHERE ";


    @Override
    public String findAll(Class<?> productClass) {
        return generateQuery(SELECT, getFields(productClass), FROM, getTableName(productClass), ";");
    }

    String generateQuery(String... args) {
        return String.join("", args);
    }

    String getFields(Class<?> productClass) {
        columnsName.clear();
        requiredEntityAnnotation(productClass);
        Field[] rawFields = productClass.getDeclaredFields();
        for (Field rawField : rawFields) {
            getColumName(rawField);
        }
        return String.join(", ", columnsName);
    }

    private void requiredEntityAnnotation(Class<?> productClass) {
        if (!isEntityAnnotationPresent(productClass)) {
            throw new RuntimeException(String.format("Provided class: %s does not contain @Entity annotation",
                    productClass.getName()));
        }
    }

    boolean isEntityAnnotationPresent(Class<?> productClass) {
        return productClass.isAnnotationPresent(Entity.class);
    }

    private void getColumName(Field rawField) {
        Column column = rawField.getAnnotation(Column.class);
        if (Objects.nonNull(column)) {
            resolveName(rawField, column.name());
        }
    }

    private void resolveName(Field rawField, String nameFromAnnotation) {
        if (nameFromAnnotation.isEmpty()) {
            columnsName.add(rawField.getName());
        } else {
            columnsName.add(nameFromAnnotation);
        }
    }

    String getTableName(Class<?> productClass) {
        String tableName;
        if (isTableAnnotationPresent(productClass)) {
            tableName = getTableNameUseAnnotation(productClass);
        } else {
            tableName = productClass.getSimpleName();
        }
        return tableName;
    }

    boolean isTableAnnotationPresent(Class<?> productClass) {
        return productClass.isAnnotationPresent(Table.class);
    }

    private String getTableNameUseAnnotation(Class<?> productClass) {
        String tableName;
        Table table = productClass.getAnnotation(Table.class);
        String content = table.name();
        if (content.length() != 0) {
            tableName = content;
        } else {
            tableName = productClass.getSimpleName();
        }
        return tableName;
    }

    @Override
    public String findById(Serializable id, Class<?> productClass) {
        return generateQuery(SELECT, getFields(productClass), FROM, getTableName(productClass),
                WHERE, getFieldIdName(productClass), "=", String.valueOf(getTypeId(id)), ";");
    }

    private Object getTypeId(Serializable idParam) {
        if (idParam instanceof Integer) {
            return (Integer) idParam;
        } else if (idParam instanceof String || idParam instanceof Date) {
            return "'" + idParam.toString() + "'";
        } else {
            return null;
        }
    }


    public String getFieldIdName(Class<?> productClass) {
        columnsName.clear();
        Field[] rawFields = productClass.getDeclaredFields();
        for (Field rawField : rawFields) {
            resolveIdName(rawField);
        }
        return String.valueOf(columnsName.get(0));
    }

    private void resolveIdName(Field rawField) {
        Id id = rawField.getAnnotation(Id.class);
        if (Objects.nonNull(id)) {
            resolveName(rawField, id.name());
        }
    }

}
