package com.tyurin.generator;

import com.tyurin.generator.annotation.Column;
import com.tyurin.generator.annotation.Entity;
import com.tyurin.generator.annotation.Id;
import com.tyurin.generator.annotation.Table;

import java.sql.Date;
@Entity
@Table(name = "product")
public class Product {
    @Id
    private Long id;
    @Column
    private String name;
    @Column
    private Double price;
    @Column
    private Date date;


    //SELECT name, price, date FROM product
}
