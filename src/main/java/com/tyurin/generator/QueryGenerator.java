package com.tyurin.generator;

import java.io.Serializable;

public interface QueryGenerator {


    String findAll(Class<?> productClass);

    String findById(Serializable id, Class<?> productClass);
}
