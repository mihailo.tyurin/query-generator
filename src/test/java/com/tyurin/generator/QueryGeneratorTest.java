package com.tyurin.generator;

import com.tyurin.generator.entitytest.ColumnsWithNames;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QueryGeneratorTest {
    private final DefaultQueryGenerator queryGenerator = new DefaultQueryGenerator();

    @Test
    void shouldReturnQueryFindAll() {
        String expectedQuery = "SELECT name, price, date FROM product;";

        String resultQuery = queryGenerator.findAll(Product.class);

        assertEquals(expectedQuery, resultQuery);
    }

    @Test
    void shouldReturnQueryFindById() {
        String expectedQuery = "SELECT name, price, date FROM product WHERE id=5;";

        QueryGenerator queryGenerator = new DefaultQueryGenerator();
        String resultQuery = queryGenerator.findById(5, Product.class);

        assertEquals(expectedQuery, resultQuery);

    }

    @Test
    void shouldReturnTrueIfTypeHasEntityAnnotation() {

        assertTrue(queryGenerator.isEntityAnnotationPresent(Product.class), "Provided class should have Entity annotation");
    }

    @Test
    void shouldThrowExceptionWhenProvidedClassDoesNotHaveEntityAnnotation() {

        assertThrows(RuntimeException.class, () -> queryGenerator.findAll(String.class));
    }

    @Test
    void shouldReturnAllColumnNameFromProvidedClass() {
        String expectedFields = "name, price, date";

        String resultFields = queryGenerator.getFields(Product.class);

        assertEquals(expectedFields, resultFields);
    }

    @Test
    void shouldReturnAllColumnNameFromProvidedClassFromAnnotation() {
        String expectedFields = "na_me, pri_ce, da_te";

        String resultFields = queryGenerator.getFields(ColumnsWithNames.class);

        assertEquals(expectedFields, resultFields);
    }

    @Test
    void shouldReturnTableNameFromProvidedClass() {
        String expectedTableName = "product";

        String resultTableName = queryGenerator.getTableName(Product.class);

        assertEquals(expectedTableName, resultTableName);
    }

    @Test
    void shouldReturnTableNameFromProvidedClassWithAnnotationName() {
        String expectedTableName = "pro_duct";

        String resultTableName = queryGenerator.getTableName(ColumnsWithNames.class);

        assertEquals(expectedTableName, resultTableName);
    }

    @Test
    void shouldReturnIdColumnNameFromProvidedClass() {
        String expectedFields = "id";
        DefaultQueryGenerator queryGenerator = new DefaultQueryGenerator();

        String resultFields = queryGenerator.getFieldIdName(Product.class);

        assertEquals(expectedFields, resultFields);
    }

}